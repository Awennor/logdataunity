﻿using Pidroh.UnityUtils.LogDataSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.Globalization;

namespace Pidroh.UnityUtils.LogDataSystem
{
    public class LogConverter
    {

        // Update is called once per frame
        void Update()
        {

        }

        internal static string ToCSV(List<LogData> logExp, char separator)
        {
            StringBuilder sB = new StringBuilder();
            sB.AppendLine("label,date,arg0,arg1,arg2,arg3,arg4,arg5".Replace(',', separator));
            const int nElements = 8;
            foreach (var lD in logExp)
            {
                string element = lD.Label + separator + 
                    lD.DateTime.ToString("yyyy-MM-ddTHH:mm:ss",CultureInfo.InvariantCulture);
                for (int i = 0; i < nElements - 2; i++)
                {
                    element += separator;
                    if (i < lD.Args.Count)
                    {
                        element += lD.Args[i];
                    }
                    else
                    {
                    }
                }
                sB.AppendLine(element);
            }
            return sB.ToString();
        }

    }
}