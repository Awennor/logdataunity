﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pidroh.UnityUtils.LogDataSystem
{

    public class LogCapturer : MonoBehaviour
    {

        /// <summary>
        /// The label identifying the events of that capturer
        /// </summary>
        [SerializeField]
        string label;
        /// <summary>
        /// A callback for when a event happens. 
        /// LogCapturer is the event origin and the list of string are the arguments of the event
        /// Usually used by the LogCapturerManager
        /// </summary>
        public event Action<LogCapturer, List<string>> OnEventHappen;

        List<string> latestArgs = new List<string>();
        List<string> aux = new List<string>();

        /// <summary>
        /// Label for the logdata of this capturers
        /// </summary>
        public string Label
        {
            get
            {
                return label;
            }

            set
            {
                label = value;
            }
        }

        /// <summary>
        /// Calls a callback for the event that happened
        /// </summary>
        public void EventHappen()
        {
            latestArgs.Clear();
            Callback();

        }

        /// <summary>
        /// Calls OnEventHappen to alert of the captured event
        /// </summary>
        private void Callback()
        {
            //Debug.Log("EVENTCALLBACK");

            if (OnEventHappen != null)
                OnEventHappen(this, latestArgs);
        }

        public void EventHappen(string s)
        {
            aux.Clear();
            aux.Add(s);
            EventHappen(aux);
        }

        public void EventHappen(string s, string s2)
        {
            aux.Clear();
            aux.Add(s);
            aux.Add(s2);
            EventHappen(aux);
        }

        public void EventHappen(string s, string s2, string s3)
        {
            aux.Clear();
            aux.Add(s);
            aux.Add(s2);
            aux.Add(s3);
            EventHappen(aux);
        }

        public void EventHappen(string[] strings)
        {
            aux.Clear();
            aux.AddRange(strings);
            EventHappen(aux);
        }

        public void EventHappen(List<string> strings)
        {
            latestArgs.Clear();
            latestArgs.AddRange(strings);
            Callback();
        }

    }

}