﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace Pidroh.UnityUtils.LogDataSystem
{
    public class LogCapturerManager : MonoBehaviour
    {


        /// <summary>
        /// A list of logdatas captured by the system
        /// </summary>
        [SerializeField]
        List<LogData> logDatas;
        /// <summary>
        /// Events to be called when an event is called
        /// </summary>
        [SerializeField]
        LogEventFilter[] events_filter;

        List<string> aux = new List<string>();

        public bool debug_ShowMessages = false;

        public List<LogData> LogDatas
        {
            get
            {
                return logDatas;
            }

            set
            {
                logDatas = value;
            }
        }


        /// <summary>
        /// Automatically fixes the name of LogCapturers based on their tag
        /// </summary>
        [ContextMenu("Automatic LogCapturer name fix")]
        public void AutomaticLogCapturerLabelToNameFix()
        {
            var logCs = GetComponentsInChildren<LogCapturer>();
            foreach (var item in logCs)
            {
                item.gameObject.name = "LogC-" + item.Label;
            }
        }


        /// <summary>
        /// Automatically fixes the label of LogCapturers based on their name
        /// </summary>
        [ContextMenu("Automatic LogCapturer name => label")]
        public void AutomaticLogCapturerNameToLabelFix()
        {
            var logCs = GetComponentsInChildren<LogCapturer>();
            foreach (var item in logCs)
            {
                string label = item.gameObject.name.Replace("LogC-", "");
                item.Label = label;
            }
        }


        /// <summary>
        /// Registers a callback on the events of each log capturer that is a child of the manager
        /// </summary>
        void Awake()
        {
            var logCs = GetComponentsInChildren<LogCapturer>();
            foreach (var item in logCs)
            {
                //Debug.Log(item.name);
                item.OnEventHappen += Item_OnEventHappen;
            }
            if (debug_ShowMessages) {
                Debug.Log("LogCapturer Setting up!");
            }
        }

        public void DirectLog(string label, params string[] list) {
            aux.Clear();
            aux.AddRange(list);
            LogData(aux, label);

        }

        /// <summary>
        /// Handles the callbacks by the log capturers. 
        /// Creates a log data whenever an event happens and adds that log data to the list.
        /// It also alerts the event system 
        /// </summary>
        /// <param name="arg1">The log capturer that fired the event</param>
        /// <param name="argList">The arguments</param>
        private void Item_OnEventHappen(LogCapturer arg1, List<string> argList)
        {
            //Debug.Log("EVENT");



            foreach (var lE in events_filter)
            {
                lE.NewLogData(arg1);
            }
            string label = arg1.Label;
            LogData(argList, label);
        }

        private void LogData(List<string> argList, string label)
        {
            var logData = new LogData(label, argList);
            logDatas.Add(logData);


            if (debug_ShowMessages)
            {
                Debug.Log("LogCapturer Event! " + logData.Label);
            }
        }
    }

    /// <summary>
    /// Calls an UnityEvent whenever a LogCapturer in it's list creates a new log data
    /// </summary>
    [Serializable]
    public class LogEventFilter
    {
        [SerializeField]
        LogCapturer[] logCapturers;
        [SerializeField]
        UnityEvent OnEvent;

        internal void NewLogData(LogCapturer logCapturer)
        {
            foreach (var lC in logCapturers)
            {
                if (lC == logCapturer)
                {

                    OnEvent.Invoke();

                    return;
                }
            }
        }
    }
}