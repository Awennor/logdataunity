﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pidroh.UnityUtils.LogDataSystem
{

    /// <summary>
    /// A single event that happens in the log
    /// The label identifies the data. 
    /// dateTime is the timestamp for the data. It's taken as soon as the logdata is created.
    /// args are the arguments of that data.
    /// </summary>
    [Serializable]
    public class LogData
    {
        [SerializeField]
        string label;
        [SerializeField]
        DateTime dateTime;
        [SerializeField]
        List<string> args = new List<string>();


        public LogData()
        {

        }

        public LogData(string label, List<string> argList)
        {
            this.label = label;
            args.AddRange(argList);
            dateTime = DateTime.Now;
        }

        public List<string> Args
        {
            get
            {
                return args;
            }

            set
            {
                args = value;
            }
        }

        public DateTime DateTime
        {
            get
            {
                return dateTime;
            }

            set
            {
                dateTime = value;
            }
        }

        public string Label
        {
            get
            {
                return label;
            }

            set
            {
                label = value;
            }
        }

    }
}