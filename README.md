# README #

### What is this repository for? ###
Simple log data system with timestamps.
If you use UnityEvents, it's possible to setup your log system without a single piece of code.

### How do I get set up? ###

Download the package and import it into Unity
https://bitbucket.org/Awennor/logdataunity/downloads/LogDataPackage.unitypackage

You can also add this project as a git submodule

### Who do I talk to? ###

We do not offer support on this system.